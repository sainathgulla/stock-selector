//
//  SearchTableViewCell.swift
//  Stock Selector
//
//  Created by mani vikash on 4/15/17.
//  Copyright © 2017 Gulla,Sainath. All rights reserved.
//

import UIKit

class SearchTableViewCell: UITableViewCell {

    @IBOutlet weak var bought: UILabel!
    @IBOutlet weak var sold: UILabel!    
    @IBOutlet weak var profit: UILabel!
    @IBOutlet weak var comm: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
   
    
}
