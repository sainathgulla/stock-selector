//
//  LoginViewController.swift
//  Stock Selector
//
//  Created by Gulla,Sainath on 4/2/17.
//  Copyright © 2017 Gulla,Sainath. All rights reserved.
//

import UIKit
import Parse
import Bolts

//Class used for user to Login into the application
class LoginViewController: UIViewController {
    //Text field for entering username
    @IBOutlet weak var usernameTXT: UITextField!
    //Text field for entering password
    @IBOutlet weak var passwordTXT: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Method for user to login into our application
    @IBAction func signIn(_ sender: Any) {        
        if (usernameTXT.text! != "" && passwordTXT.text! != "") {
            PFUser.logInWithUsername(inBackground: usernameTXT.text!,
                                     password: passwordTXT.text!,
                                     block:
                {(user, error) -> Void in
                    if error != nil{
                        //print(error)
                        self.displayAlertWithTitle("Login Failure!", message:"Username or Password Mismatch")
                    } else if user != nil {
                        self.performSegue(withIdentifier: "home", sender: nil)
                    }
                    /*
                     else {
                     // Everything went alright here
                     self.displayAlertWithTitle("Success!", message:"Login successful")
                     self.performSegue(withIdentifier: "signup", sender: nil)
                     }
                     */
            })
        }
        else {
            self.displayAlertWithTitle("Unsuccess!", message:"Please enter Username and Password to login")
        }
    }
    
    //Method for displaying alert box
    func displayAlertWithTitle(_ title:String, message:String) {
        let alert = UIAlertController(title: title, message: message,
                                      preferredStyle: .alert)
        let defaultAction = UIAlertAction(title:"OK",
                                          style: .default, handler: nil)
        alert.addAction(defaultAction)
        self.present(alert,animated:true, completion:nil)
        
    }
    
    //Method for unwinding back to login page
    @IBAction func unwindToLogin( login: UIStoryboardSegue ) {
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let tapper = UITapGestureRecognizer(target: self, action:#selector(SignUpViewController.dismissKeyboard))
        tapper.cancelsTouchesInView = false
        view.addGestureRecognizer(tapper)
    }
    
    //Method for dismissing the keyboard
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */    
}
