//
//  StockSuggestionTableViewController.swift
//  Stock Selector
//
//  Created by Gulla,Sainath on 4/13/17.
//  Copyright © 2017 Gulla,Sainath. All rights reserved.
//

import UIKit
import Parse
import Bolts

//Class used for displaying history of stock results
class StockSuggestionTableViewController: UITableViewController {
    var stocks:[PFObject] = []
    var cmpnames : [String] = []
    var prolist : [Double] = []
    var fin : [String:Double] = [:]
    var countfreq : [String:Double] = [:]

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = UITableViewAutomaticDimension
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        cmpnames = []
        prolist = []
        fin = [:]
        countfreq = [:]
        parsecompany()
    }

    // MARK: - Table view data source
    
    //Method fornumber of sections
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    //Method for number of rows
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return fin.count
    }

    //Method for displaying data in cell
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "sample", for: indexPath)
        // Configure the cell...
        cell.textLabel?.text = String(Array(self.fin.keys)[indexPath.row])
        cell.detailTextLabel?.text = String(format:"%3.2f", Array(self.fin.values)[indexPath.row])
        return cell
    }
    
    //Method for calculating profit percentage of stocks
    func parsecompany()->[String]{
        let query = PFQuery(className:"Stock")
        query.findObjectsInBackground(block: { (objects : [PFObject]?, error: Error?) -> Void in
            if error == nil {
                self.stocks = objects!
                for stock in self.stocks {
                    let bv = stock["bought"] as! Double
                    let sv = stock["sold"] as! Double
                    let profit = (sv-bv) / bv * 100
                    self.prolist.append(profit)
                    self.cmpnames.append(stock["stockCompany"] as! String)
                }
                print("inside function : \(self.cmpnames) ")
                for i in 0...self.cmpnames.count-1{
                    if(self.fin.keys.contains(self.cmpnames[i])){                                               self.fin[self.cmpnames[i]] = self.fin[self.cmpnames[i]]!+self.prolist[i]
                        self.countfreq[self.cmpnames[i]] = self.countfreq[self.cmpnames[i]]! + 1
                    } else {
                        self.fin[self.cmpnames[i]] = self.prolist[i]
                        self.countfreq[self.cmpnames[i]] = 1
                    }
                    print(self.fin)
                }
                for (key, _) in self.fin{
                    self.fin[key] = self.fin[key]!/self.countfreq[key]!
                }
                self.tableView.reloadData()
            } else {
                // Log details of the failure
                //self.displayAlertWithTitle("Oops", message: "\(error!) \(error!._userInfo)")
                print("failure")
            }
        })
        print("overall function : \(cmpnames) ")
        return cmpnames
    }
    
    //Method for displaying background color of cells based on profit percentage
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch Array(self.fin.values)[indexPath.row] {
        case let x where x < 0.0:
            cell.backgroundColor = UIColor(red: 255.0/255.0, green: 59.0/255.0, blue: 48.0/255.0, alpha: 1.0)
        case let x where x > 0.0:
            cell.backgroundColor = UIColor(red: 76.0/255.0, green: 217.0/255.0, blue: 100.0/255.0, alpha: 1.0)
        default:
            cell.backgroundColor = UIColor(red: 44.0/255.0, green: 186.0/255.0, blue: 231.0/255.0, alpha: 1.0)
        }
        cell.textLabel?.textColor = UIColor.white
        cell.detailTextLabel?.textColor = UIColor.white
        cell.textLabel?.font = UIFont(name: "HelveticaNeue-CondensedBold", size: 20)
        cell.detailTextLabel?.font = UIFont(name: "HelveticaNeue-CondensedBold", size: 20)
        cell.textLabel?.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25)
        cell.textLabel?.shadowOffset = CGSize(width: 0, height: 1)
        cell.detailTextLabel?.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25)
        cell.detailTextLabel?.shadowOffset = CGSize(width: 0, height: 1)
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
