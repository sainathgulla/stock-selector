//
//  StockSearchViewController.swift
//  Stock Selector
//
//  Created by Gulla,Sainath on 4/2/17.
//  Copyright © 2017 Gulla,Sainath. All rights reserved.
//

import UIKit
import Parse
import Bolts

//Class used for user for finding the stock results
class StockSearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    //Text field for entering stock sector
    @IBOutlet weak var searchCompanyTXT: UITextField!
    //Text field for entering stock company
    @IBOutlet weak var table: UITableView!
    
    //Method for returning number of sections in a table
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    //Method for returning number of rows in a section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return companynames.count
    }
    
    //Method for displaying data in cells
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! SearchTableViewCell
        // Configure the cell...
        cell.bought.text = String(format:"Bought: $%3.0f", self.bou[indexPath.row])
        cell.sold.text = String(format:"Sold: $%3.0f", self.sol[indexPath.row])
        cell.profit.text = String(format:"Profit: %3.0f%%", self.prof[indexPath.row])
        cell.comm.text = String(self.comnt[indexPath.row])
        if(indexPath.row % 2 == 0) {
            cell.backgroundColor = UIColor.brown
        } else {
             cell.backgroundColor = UIColor.gray
        }
        return cell
    }
    
    var stocks:[PFObject] = []
    var companynames:[String] = []
    var bou:[Double] = []
    var sol:[Double] = []
    var comnt:[String] = []
    var prof:[Double] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
       NotificationCenter.default.addObserver(self, selector: Selector(("reloadTableData:")), name: NSNotification.Name(rawValue: "reload"), object: nil)
    }
    
    //Method for reloading the table data
    func reloadTableData(notification: NSNotification) {
        table.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.searchCompanyTXT.text = ""
        NotificationCenter.default.addObserver(self, selector: Selector(("reloadTableData:")), name: NSNotification.Name(rawValue: "reload"), object: nil)
        let tapper = UITapGestureRecognizer(target: self, action:#selector(StockSearchViewController.dismissKeyboard))
        tapper.cancelsTouchesInView = false
        view.addGestureRecognizer(tapper)
    }
    
    //Method for dismissing the keyboard
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    //Method for searching compsny stock results
    @IBAction func searchCompanyResults(_ sender: Any) {
        self.companynames = []
        self.bou = []
        self.sol = []
        self.prof = []
        self.comnt = []
        
        if (searchCompanyTXT.text! != "") {
        let query = PFQuery(className:"Stock")
        if searchCompanyTXT.text!.characters.count > 0 {
            query.whereKey("stockCompany", equalTo:searchCompanyTXT.text!)
        } else {
            query.whereKeyExists("stockCompany") // just fetch objects with this key (which is all of them)
        }
        query.findObjectsInBackground(block: { (objects : [PFObject]?, error: Error?) -> Void in
            if error == nil {
                // The find succeeded.
                self.displayAlertWithTitle("Success!", message:"Retrieved \(objects!.count) stocks.")
                self.stocks = objects!
                for stock in self.stocks {
                    // Do something with the found objects
                    self.companynames.append(stock["stockCompany"]! as! String)
                    self.bou.append(stock["bought"]! as! Double)
                    self.sol.append(stock["sold"]! as! Double)
                    self.prof.append((((stock["sold"]! as! Double)-(stock["bought"]! as! Double))/(stock["bought"]! as! Double))*100)
                    self.comnt.append(stock["comments"] as! String)
                }
                print("names list \(self.companynames)")
                print("bougt list \(self.bou)")
                self.table.reloadData()
            } else {
                // Log details of the failure
                self.displayAlertWithTitle("Oops", message: "\(error!) \(error!._userInfo)")
            }
        })
        } else {
            self.displayAlertWithTitle("Unsuccess!", message:"Please enter company name")
        }
    }
    
    //Method for displaying the alert box
    func displayAlertWithTitle(_ title:String, message:String){
        let alert:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let defaultAction:UIAlertAction =  UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(defaultAction)
        self.present(alert, animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
