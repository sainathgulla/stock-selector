//
//  StockFeedbackViewController.swift
//  Stock Selector
//
//  Created by Gulla,Sainath on 4/2/17.
//  Copyright © 2017 Gulla,Sainath. All rights reserved.
//

import UIKit
import Parse
import Bolts

//Class which is used for user to submit feedback feedback about stocks
class StockFeedbackViewController: UIViewController {
    //Text field for entering stock sector
    @IBOutlet weak var stockSectorTXT: UITextField!
    //Text field for entering stock company
    @IBOutlet weak var stockCompanyTXT: UITextField!
    //Text field for entering bought price
    @IBOutlet weak var boughtTXT: UITextField!
    //Text field for entering sold price
    @IBOutlet weak var soldTXT: UITextField!
    //Text field for entering comments
    @IBOutlet weak var commentTXT: UITextView!
    
    var stocks: [PFObject] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Method for users to submit the feedback
    @IBAction func submitFeedback(_ sender: Any) {
        let stock = PFObject(className: "Stock")
        if (stockSectorTXT.text! != "" && stockCompanyTXT.text!
!= "" && boughtTXT.text! != "" && soldTXT.text! != "" && commentTXT.text! != "") {
        stock["sector"] = stockSectorTXT.text
        stock["stockCompany"] = stockCompanyTXT.text
        stock["bought"] = Double(boughtTXT.text!)!
        stock["sold"] = Double(soldTXT.text!)!
        stock["comments"] = commentTXT.text
        
        stock.saveInBackground(block: { (success, error) -> Void in
            print("Stocks has been saved.")
            self.displayAlertWithTitle("Success!", message:"Stocks Feedback has been saved successfully")
            self.stockSectorTXT.text = ""
            self.stockCompanyTXT.text = ""
            self.boughtTXT.text = ""
            self.soldTXT.text = ""
            self.commentTXT.text = ""
        })
    } else {
            self.displayAlertWithTitle("Unsuccess!", message:"All fields are mandatory")
        }
    }
 
    //Method for displaying the alert box
    func displayAlertWithTitle(_ title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title:"OK", style: .default, handler: nil)
        alert.addAction(defaultAction)
        self.present(alert,animated:true, completion:nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.stockSectorTXT.text = ""
        self.stockCompanyTXT.text = ""
        self.boughtTXT.text = ""
        self.soldTXT.text = ""
        self.commentTXT.text = ""
        
        let tapper = UITapGestureRecognizer(target: self, action:#selector(StockFeedbackViewController.dismissKeyboard))
        tapper.cancelsTouchesInView = false
        view.addGestureRecognizer(tapper)
    }
    
    //Method for dismissing the keyboard
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
