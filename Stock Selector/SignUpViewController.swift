//
//  SignUpViewController.swift
//  Stock Selector
//
//  Created by Gulla,Sainath on 4/2/17.
//  Copyright © 2017 Gulla,Sainath. All rights reserved.
//

import UIKit
import Parse
import Bolts

//Class used for user to SignUp into the application
class SignUpViewController: UIViewController {
    //Text field for entering firstname
    @IBOutlet weak var firstNameTXT: UITextField!
    //Text field for entering lastname
    @IBOutlet weak var lastNameTXT: UITextField!
    //Text field for entering username
    @IBOutlet weak var usernameTXT: UITextField!
    //Text field for entering email
    @IBOutlet weak var emailTXT: UITextField!
    //Text field for entering password
    @IBOutlet weak var passwordTXT: UITextField!
    //Text field for re-entering password
    @IBOutlet weak var reTypePasswordTXT: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let tapper = UITapGestureRecognizer(target: self, action:#selector(SignUpViewController.dismissKeyboard))
        tapper.cancelsTouchesInView = false
        view.addGestureRecognizer(tapper)
    }
    
    //Method for dismissing keyboard
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.        
        view.endEditing(true)
    }
    
    //Method for user's to signup for the application
    @IBAction func signUp(_ sender: Any) {
        // Defining the user object
        let user = PFUser()
        user["firstName"] = firstNameTXT.text!
        user["lastName"] = lastNameTXT.text!
        user.username = usernameTXT.text! //usernameTF.text! user.password = passwordTF.text!
        user.email = emailTXT.text!
        user.password = passwordTXT.text!
        if passwordTXT.text! != "" || reTypePasswordTXT.text! != "" {
            if self.passwordTXT.text! != self.reTypePasswordTXT.text! {
                self.displayAlertWithTitle("Password mismatch", message: "Please re-type password")
            } else {
                user.signUpInBackground( block: {
                    (success, error) -> Void in
                    if let error = error as NSError? {
                        let errorString = error.userInfo["error"] as? NSString
                        // In case something went wrong, use errorString to get the error
                        self.displayAlertWithTitle("Something has gone wrong", message:"\(errorString)")
                    } else {
                        // Everything went okay
                        self.displayAlertWithTitle("Success!", message:"Registration was successful")
                        let emailVerified = user["emailVerified"]
                        if emailVerified != nil && (emailVerified as! Bool) == true {
                            // Everything is fine
                        } else {
                            // The email has not been verified, so logout the user
                            PFUser.logOut()
                        }
                    } })
            }
        } else {
            displayAlertWithTitle("Sorry", message: "Please Enter Email and password")
        }
    }
    
    //Method for displaying the alert box
    func displayAlertWithTitle(_ title:String, message:String){
        let alert:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let defaultAction:UIAlertAction =  UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(defaultAction)
        self.present(alert, animated: true, completion: nil)        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
