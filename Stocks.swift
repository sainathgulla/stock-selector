//
//  Stocks.swift
//  Stock Selector
//
//  Created by Gulla,Sainath on 4/2/17.
//  Copyright © 2017 Gulla,Sainath. All rights reserved.
//

import Foundation
import Parse
import Bolts

class Stock:PFObject, PFSubclassing {
    @NSManaged var sector:String
    @NSManaged var stockCompany:String
    @NSManaged var bought:Double
    @NSManaged var sold:Double
    @NSManaged var comments:String
    
    static func parseClassName() -> String {
        return "Stock"
    }
}
